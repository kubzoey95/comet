﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loose : MonoBehaviour
{
    public float no_rotate_time;
    public float size;
    private bool lost;

    // Use this for initialization
    void Start()
    {
        lost = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.localScale.x < size || gameObject.GetComponent<rotor>().GetNoRotateTime() > no_rotate_time)
        {
            GameObject[] objs = Resources.FindObjectsOfTypeAll<GameObject>();
            foreach (GameObject obj in objs)
            {
                if (obj.name == "Lost" && obj.layer == 5 && !lost)
                {
                    Vector3 vel = gameObject.GetComponent<rotor>().GetVel();
                    gameObject.GetComponent<Rigidbody>().useGravity = true;
                    gameObject.GetComponent<rotor>().enabled = false;
                    gameObject.GetComponent<Rigidbody>().velocity = vel;
                    gameObject.GetComponent<TrailRenderer>().enabled = false;
                    obj.SetActive(true);
                    lost = true;
                }
            }
        }
    }
}
