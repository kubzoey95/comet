﻿using UnityEngine;

public class takeRandomMesh : MonoBehaviour
{
    void Start()
    {
        GameObject from = GameObject.Find("SceneLoader");
        this.GetComponent<MeshFilter>().mesh = from.GetComponent<loadMeshes>().meshes[Random.Range(0, from.GetComponent<loadMeshes>().meshes.Count)];
    }
}
