﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pixelScale : MonoBehaviour
{
    public Camera cam;

    Texture tex;
    void Start()
    {
        tex = this.GetComponent<MeshRenderer>().material.mainTexture;
    }

    void Update()
    {
        //Debug.Log(new Vector2(tex.width, tex.height));
        float coefX = cam.orthographicSize * cam.pixelWidth * 2f;
        float coefY = cam.orthographicSize * cam.pixelHeight * 2f;
        Debug.Log(new Vector3(coefX * tex.width, coefY * tex.height, 1f));
        //transform.localScale = new Vector3(coefX * tex.width, coefY * tex.height, 1f);
    }
}
