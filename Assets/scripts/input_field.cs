﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class input_field : MonoBehaviour {

    private void Update()
    {

    }

    private void OnEnable()
    {
        gameObject.GetComponent<InputField>().text = "0";
    }

    public void VerifyLevel(GameObject play_button)
    {
        if (gameObject.GetComponent<InputField>().text.Length > 0) {
            int lvl = 0;
            int.TryParse(gameObject.GetComponent<InputField>().text, out lvl);
            if (lvl > GameObject.Find("statistics").GetComponent<stat>().GetLevel() || lvl < 0)
            {
                play_button.SetActive(false);
            }
            else
            {
                play_button.SetActive(true);
            }
        }
        else
        {
            play_button.SetActive(false);
        }
    }
}